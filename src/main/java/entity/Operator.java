package entity;

public class Operator extends Employee {

    public Operator(final String nombre) {
        super(nombre);
        this.setPriority(1);
        this.setRole("operador");
    }

}
