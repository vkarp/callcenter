package entity;

public class Director extends Employee {
    public Director(String nombre) {
        super(nombre);
        this.setPriority(3);
        this.setRole("director");
    }
}
