package entity;

import callcenter.Call;

public abstract class Employee implements Comparable<Employee>{

    private Integer priority;
    private String nombre;
    private String role;
    private EMPLOYEE_STATUS status = EMPLOYEE_STATUS.STATUS_ACTIVE;
    private Call call;


    public Employee(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getNombre() {
        return nombre;
    }

    public String getRole() {
        return role;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public EMPLOYEE_STATUS getStatus() {
        return status;
    }

    public void setStatus(EMPLOYEE_STATUS status) {
        this.status = status;
    }

    public Call getCall() {
        return call;
    }

    public void setCall(Call call) {
        this.call = call;
    }

    @Override
    public String toString() {
        return "entity.Employee{" +
                "priority=" + priority +
                ", nombre='" + nombre + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (!nombre.equals(employee.nombre)) return false;
        return role.equals(employee.role);

    }

    public enum EMPLOYEE_STATUS {
        STATUS_ACTIVE("Active"),
        STATUS_INACTIVE("Inactive");

        private String status;

        EMPLOYEE_STATUS(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }

    @Override
    public int compareTo(Employee employee) {
        return this.getPriority() - employee.getPriority();
    }

    public void processCall(){

       System.out.println(String.format("Usted fue atendido por=[%s], rol=[%s]. Tiempo de atencion de la llamada id=[%s] es de [%s ms]. Thread ID=[%s]",
                getNombre(),getRole(),call.getCallId(),call.getDuration(), Thread.currentThread().getId()));

    }

}
