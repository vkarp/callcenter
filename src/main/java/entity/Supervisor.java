package entity;

public class Supervisor extends Employee {

    public Supervisor(final String nombre) {
        super(nombre);
        this.setPriority(2);
        this.setRole("supervisor");
    }

}
