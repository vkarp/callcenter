package callcenter;

import entity.Director;
import entity.Employee;
import entity.Operator;
import entity.Supervisor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/*
    La clase priincipal que simula ser un call center.
    Las posibles opciones son:
    - simular un llamado
    - agregar un empleado(operador, supervisor, director)
    - terminar la ejecucion del programa
 */

public class CallCenter {
    public static void main(String[] args) throws InterruptedException, IOException {

        final String COMM_FIN =                     "fin";
        final String COMM_SIMULATE_CALL =           "1";
        final String COMM_ADD_EMPLOYEE =            "2";
        final String COMM_ROL_EMPLOYEE_DIRECTOR =   "1";
        final String COMM_ROL_EMPLOYEE_SUPERVISOR = "2";
        final String COMM_ROL_EMPLOYEE_OPERADOR =   "3";
        final String COMM_REGRESAR =                "5";

        //los empleados iniciales
        List<Employee> employees = Dispatcher.createInitialEmployees();

        employees.forEach(employee -> System.out.println(employee));

        //la clase principal que despacha los llamados
        Dispatcher dispatcher = new Dispatcher(employees);

        //inicializamos los jobs(threads)
        dispatcher.initJobs(10);

        boolean wait = true;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while (wait){
            System.out.println("###### ELIJA UNA OPCION PARA CONTINUAR ######");
            System.out.println("###### Terminar: .................. [fin]");
            System.out.println("###### Simular Llamado: ............[1]");
            System.out.println("###### Agregar Empleado: ...........[2]");

            String comando = br.readLine();

            switch (comando){

                case COMM_FIN:
                    dispatcher.terminate();
                    wait = false;
                    break;

                case COMM_SIMULATE_CALL:
                    System.out.println("Despachando llamada" );
                    dispatcher.dispatchCall(new Call());
                    break;

                case COMM_ADD_EMPLOYEE:
                    boolean waitRol = true;
                    String commRol = "3";
                    while (waitRol){
                        final String[] ops = new String[]{"1","2","3", "5", "FIN"};
                        System.out.println("###### Elija un ROL ######");
                        System.out.println("###### Director ............[1]");
                        System.out.println("###### Supervidor ..........[2]");
                        System.out.println("###### Operador ............[3]");
                        System.out.println("###### Salir Del Menu ......[5]");

                        commRol = br.readLine();

                        if(contains(ops,commRol)){
                            waitRol = false;
                        }

                    }

                    if(COMM_REGRESAR.equalsIgnoreCase(commRol)){
                        break;
                    }

                    System.out.println("###### INGRESE EL NOMBRE ######");

                    String emplNombre = br.readLine();

                    switch (commRol){
                        case COMM_ROL_EMPLOYEE_DIRECTOR:
                            dispatcher.addNewEmployee(new Director(emplNombre));
                            break;

                        case COMM_ROL_EMPLOYEE_SUPERVISOR:
                            dispatcher.addNewEmployee(new Supervisor(emplNombre));
                            break;

                        case COMM_ROL_EMPLOYEE_OPERADOR:
                            dispatcher.addNewEmployee(new Operator(emplNombre));
                            break;
                    }
                    break;

                default:
                    System.out.println("###### HA INGRESADO UN COMANDO INCORRECTO!");
                    break;
            }

        }
        System.out.println("#######################################################");
        System.out.println("#### Total Llamadas Atendidas: "+ Call.count);
        System.out.println("#######################################################");
        System.out.println("############### FIN PROGRAMA! #########################");
        System.out.println("#######################################################");
    }

    public static <T> boolean contains(final T[] array, final T v) {
        for (final T e : array)
            if (e == v || v != null && v.equals(e))
                return true;

        return false;
    }

}
