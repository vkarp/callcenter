package callcenter;

import entity.Director;
import entity.Employee;
import entity.Operator;
import entity.Supervisor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

public class Dispatcher implements IDispatcher{

    private List<Job> jobs = null;
    private List<Employee> employees;


    public Dispatcher(List<Employee> employees) {
        this.employees = employees;
    }

    public void dispatchCall(final Call call) throws InterruptedException {

        final Job job = getAvailableJob();              //buscamos un job disponible

        final Employee empl = getAvailableEmployee();   //buscamos un empleado disponible

        System.out.println("Sera atendido por: " + empl.getNombre() + ", rol: "+empl.getRole());

        long duration = ThreadLocalRandom.current().nextLong(5,10) * 1000; //para pasar a ms

        call.setDuration(duration);//seteamos la duracion de la call

        empl.setCall(call);

        empl.setStatus(Employee.EMPLOYEE_STATUS.STATUS_INACTIVE); //pasamos el empl a estado no disponible

        job.assignOperator(empl); //asiganamos al job el operador

//        conectamos el llamado con el operador
        if(job.getCurrentThread()!=null && job.getSuspended()){
            job.resume();
        }

    }

    public List<Job> initJobs(int initialCapacity){

        if(this.jobs!=null){
            return this.jobs;
        }

        //Inicializa los jobs
        this.jobs = new ArrayList<>(initialCapacity);

        for(int i = 0; i< initialCapacity ; i++) {
            final Job aJob = new Job("callcenter.Job id: "+ i);
            aJob.start();
            System.out.println("Creando thread id: "+ aJob.getCurrentThread().getId());
            this.jobs.add(aJob);
        }

        return this.jobs;

    }

    public Employee getAvailableEmployee(){

        Employee employee = getActiveEmployee();

        while(employee==null){

            employee = getActiveEmployee();

            if (employee == null) {
                System.out.println("Todos los operadores estan ocupados...intentando comunicar");
            }
        }

        return employee;
    }

    private Employee getActiveEmployee(){

        Employee employee = null;

        Optional<Employee> optional = this.employees.stream().filter(emp-> Employee.EMPLOYEE_STATUS.STATUS_ACTIVE.getStatus().equalsIgnoreCase(emp.getStatus().getStatus())).findFirst();

        if(optional.isPresent()){
            employee = optional.get();
        }

        return employee;
    }

    private Job getAvailableJob() {
        Job job = getActiveJob();

        while(job==null){
            job = getActiveJob();

        }
        return job;
    }

    private Job getActiveJob() {
        Job job = null;
        Optional<Job> oJob = this.jobs.stream().filter(ajob -> ajob.getSuspended()).findFirst();
        if(oJob.isPresent()){
            job = oJob.get();
        }
        return job;
    }




    public List<Job> getJobs() {
        return jobs;
    }

    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public synchronized void terminate() {

            boolean finThreads = false;

            while(!finThreads) {
                this.getJobs().forEach(job -> {
//                    try {

                        if(job.getCurrentThread().getState().equals(Thread.State.WAITING)){
                            job.resume();
                            job.setRunning(false);
                        }

                });

                finThreads = this.getJobs().stream().allMatch(job -> job.getCurrentThread().getState().equals(Thread.State.TERMINATED));;

            }

        this.getJobs().stream().forEach(job -> System.out.println(job));


    }

    public void addNewEmployee(Employee empl){
        this.employees.add(empl);
        Collections.sort(this.employees);
    }

    public static List<Employee> createInitialEmployees(){

        List<Employee> employees = new ArrayList<>();
        employees.add(new Operator("Diego"));
        employees.add(new Operator("Facundo"));
        employees.add(new Operator("Leandro"));
        employees.add(new Operator("Martina"));
        employees.add(new Operator("Juana"));
        employees.add(new Operator("Valentina"));
        employees.add(new Operator("Ramon"));
        employees.add(new Supervisor("Julieta"));
        employees.add(new Supervisor("Valeria"));
        employees.add(new Director("Chelita"));

        Collections.sort(employees);

        return employees;
    }
}