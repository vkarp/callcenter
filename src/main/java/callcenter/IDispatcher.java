package callcenter;

public interface IDispatcher {
    public void dispatchCall(Call call) throws InterruptedException;

}
