package callcenter;

import entity.Employee;

import java.util.concurrent.ThreadLocalRandom;

public class Job implements Runnable {
    private String threadName;
    boolean running = true;
    private Thread currentThread;
    private Employee operator = null;
    private boolean suspended = true;

    public Job(final String tName) {
        this.threadName = tName;
    }

    @Override
    public void run() {
        try {
            while(running){

                synchronized(this) {
                    while(suspended) {
                        wait();
                    }
                }

                if(this.getOperator()!=null){

                    Thread.sleep(operator.getCall().getDuration());

                    operator.processCall();

                    this.operator.setStatus(Employee.EMPLOYEE_STATUS.STATUS_ACTIVE);

                    this.suspend();

                    this.assignOperator(null);

                } else if(!this.running){
                    break;
                }

            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public synchronized void setRunning(boolean running) {
        this.running = running;
        notify();
    }

    public boolean isRunning() {
        return running;
    }

    public Thread getCurrentThread() {
        return currentThread;
    }

    public void setCurrentThread(Thread currentThread) {
        this.currentThread = currentThread;
    }

    public Employee getOperator() {
        return operator;
    }

    public void setOperator(Employee operator) {
        this.operator = operator;
    }

    public void start () {
        if (currentThread == null) {
            currentThread = new Thread (this, threadName);
            currentThread.start ();
        }
    }

    void suspend() {
//        System.out.println("suspend del job(thread) id: " +  this.getCurrentThread().getId() );
        suspended = true;
    }

    synchronized void resume() {
//        System.out.println("resume del job(thread) id: " +  this.getCurrentThread().getId() );
        suspended = false;
        notify();
    }

    synchronized void assignOperator(Employee op){
        this.operator = op;
        notify();
    }

    public boolean getSuspended() {
        return suspended;
    }

    public String getThreadName() {
        return threadName;
    }

    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }

    @Override
    public String toString() {
        return "Job{" +
                "threadName='" + threadName + '\'' +
                ", running=" + running +
                ", currentThread=" + currentThread +
                ", operator=" + operator +
                ", suspended=" + suspended +
                ", thread id="+ currentThread.getId() +
                '}';
    }
}
