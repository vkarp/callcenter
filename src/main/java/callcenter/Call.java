package callcenter;

import java.util.Date;

public class Call {

    private Integer callId;

    private Date fecha;

    private long duration;

    public static Integer count = 0;

    public Integer getCallId() {
        return callId;
    }

    public void setCallId(Integer callId) {
        this.callId = callId;
    }

    public Call() {
        count++;
        this.callId = count;
        this.fecha = new Date();
    }
    public Call(Integer callId) {
        count++;
        this.callId = callId;
        this.fecha = new Date();
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

}
