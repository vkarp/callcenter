package dispatcher;

import callcenter.Call;
import callcenter.Dispatcher;
import entity.Employee;
import org.junit.Test;

import java.util.List;

public class TestDispatcherJunit {

    @Test
    public void testDispatcher() throws InterruptedException {
        List<Employee> employees = Dispatcher.createInitialEmployees();
        employees.forEach(employee -> System.out.println(employee));
        Dispatcher dispatcher = new Dispatcher(employees);
        dispatcher.initJobs(10);

        //100 llamados y vemos el coportamiento por consola
        for(int i = 1; i<11; i++) {
            System.out.println("Despachando llamada N° "+i );
            dispatcher.dispatchCall(new Call(i));
        }

        //sleep de 20 segundos
        Thread.sleep(20*1000);

        //Termina todos los hilos y finaliza el programa
        dispatcher.terminate();

        System.out.println("#######################################################");
        System.out.println("#### Total Llamadas Atendidas: "+ Call.count);
        System.out.println("#######################################################");
        System.out.println("############### FIN PROGRAMA! #########################");
        System.out.println("#######################################################");

    }
}
